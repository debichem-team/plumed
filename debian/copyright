Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plumed
Source: http://www.plumed-code.org/get-it

Files: *
Copyright: 2012-2015 The plumed team
License: LGPL-3.0+

Files: src/lapack/lapack.cpp, src/blas/blas.cpp
Copyright: 2012-2013 GROMACS development team
License: LGPL-2.0+

Files: src/lapack/lapack.h, src/blas/blas.h
Copyright: 1991-2000 University of Groningen
           2001-2008,2012,2013 GROMACS development team
License: LGPL-2.0+

Files: src/molfile/*
Copyright: 2003 Theoretical and Computational Biophysics Group
           1995-2009 The Board of Trustees of the 
                     University of Illinois
License: University of Illinois Open Source License

Files: debian/*
Copyright: 2015 Michael Banck <mbanck@lightning.caipicrew.dd-dns.de>
License: LGPL-3.0+

License: LGPL-3.0+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

License: LGPL-2.0+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: University of Illinois Open Source License
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the Software), to deal with
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to
 do so, subject to the following conditions:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimers.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimers in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the names of Theoretical and Computational Biophysics Group,
 University of Illinois at Urbana-Champaign, nor the names of its contributors
 may be used to endorse or promote products derived from this Software without
 specific prior written permission.
 .
 THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS WITH THE SOFTWARE.
